# Referenced from https://github.com/dita-ot/dita-ot/blob/3.5.2/Dockerfile
FROM adoptopenjdk:11-jdk-hotspot

RUN apt-get update \
    && apt-get install -y unzip git \
    && rm -rf /var/lib/apt/lists/*

#ARG VERSION
ENV VERSION 3.5.2
WORKDIR /tmp
RUN git clone https://gitlab.com/bencedar/dita-ot-mirror.git && \
    cd dita-ot-mirror && \
    git checkout tags/$VERSION && \
    git submodule update --init --recursive && \
    ./gradlew && \
    ./gradlew dist -x :generateJavadocs && \
    unzip /tmp/dita-ot-mirror/build/distributions/dita-ot-$VERSION.zip -d /tmp/ && \
    cd /tmp && \
    rm -rf /tmp/dita-ot-mirror && \
    mkdir -p /opt/app/ && \
    mv /tmp/dita-ot-$VERSION/bin /opt/app/bin && \
    chmod 755 /opt/app/bin/dita && \
    mv /tmp/dita-ot-$VERSION/config /opt/app/config && \
    mv /tmp/dita-ot-$VERSION/lib /opt/app/lib && \
    mv /tmp/dita-ot-$VERSION/plugins /opt/app/plugins && \
    mv /tmp/dita-ot-$VERSION/build.xml /opt/app/build.xml && \
    mv /tmp/dita-ot-$VERSION/integrator.xml /opt/app/integrator.xml && \
    rm -r /tmp/dita-ot-$VERSION && \
    /opt/app/bin/dita --install

RUN useradd -ms /bin/bash dita-ot && \
    chown -R dita-ot:dita-ot /opt/app
USER dita-ot

ENV DITA_HOME=/opt/app
ENV PATH=${PATH}:${DITA_HOME}/bin
WORKDIR $DITA_HOME
ENTRYPOINT ["/opt/app/bin/dita"]
