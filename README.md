# markdown-dita-report

To use you can either log into GitHub and Pull the official dita docker image follow their [instructions](https://www.dita-ot.org/3.5/topics/using-docker-images.html), which involves logging into github through docker to access the package (dumb, i know)

OR....

You can use the Dockerfile that I modified referencing theirs by running the following...

```bash
docker build -t localhost/dita-ot .
```

and build your files running...

```bash
docker run --rm -it -v ${PWD}:/src localhost/dita-ot -i /src/report.ditamap -o /src/out -f pdf -v
```

## Markdown DITA syntax

https://www.dita-ot.org/dev/topics/markdown-dita-syntax-reference.html

## Source Content

The content of the source was pulled from the internet.  I searched a few different keywords and landed on something that looked pretty good.  Will be using it for formatting purposes only.

PDF is here at [src_file.pdf](src_file.pdf).