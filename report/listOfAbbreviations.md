# List of Initialisms and Acronyms Used in Report {.reference}

ADM
:   Assistant Deputy Minister

AEB
:   Audit and Evaluation Branch


CIO
:   Chief Information Office

CIPO
:   Canadian Intellectual Property Office

CMS
:   Corporate Management Sector

CSD
:   Corporate Services Directorate

DG
:   Director General

DSO
:   Departmental Security Officer

DSR
:   Desktop Software Renewal

GC
:   Government of Canada

HEAT
:   Helpdesk Expert Automation Tool

IC
:   Industry Canada

IFMS
:   Integrated Financial and Material System

IT
:   Information Technology

MS
:   Microsoft

OIC
:   Order In Council

ORBITT
:   Organizational Renewal and Business IT Transformation

PMM
:   Plant Maintenance Module

RCM
:   Responsibility Centre Manager

RVD
:   Request for Volume Discount

SITT
:   Spectrum, Information Technologies and Telecommunications Sector

SSC
:   Shared Services Canada

SSD
:   Security Services Directorate

TB
:   Treasury Board of Canada