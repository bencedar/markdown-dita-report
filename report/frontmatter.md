# 

This publication is also available online at: http://www.ic.gc.ca/eic/site/ae-ve.nsf/eng/h_03776.html

To obtain a copy of this publication or an alternate format (Braille, large print, etc.), please fill out the Publication Request Form at www.ic.gc.ca.Publication-Request or contact the:

<lines>
Web Services Centre
Industry Canada  
C.D. Howe Building  
235 Queen Street  
Ottawa, ON K1A 0H5  
Canada
</lines>

<lines>
Telephone (toll-free in Canada): 1-800-328-6189  
Telephone (Ottawa): 613-954-5031
TTY (for hearing-impaired): 1-866-694-8389  
Business hours: 8:30 a.m. to 5:00 p.m. (Eastern Time)  
Email: info@ic.gc.ca
</lines>

**Permission to Reproduce**

Except as otherwise specifically noted, the information in this publication may be reproduced, in part or in whole and by any means, without charge or further permission from Industry Canada, provided that due diligence is exercised in ensuring the accuracy of the information reproduced; that Industry Canada is identified as the source institution; and that the reproduction is not represented as an official version of the information reproduced, nor as having been made in affiliation with, or with the endorsement of, Industry Canada.

For permission to reproduce the information in this publication for commercial purposes, please fill out the Application for Crown Copyright Clearance at www.ic.gc.ca/copyright-request or contact the Web Services Centre (see contact information above).

© Her Majesty the Queen in Right of Canada, as represented by the Minister of Industry, 2015

<lines>
Cat. No. Iu4-166/2015E-PDF
ISBN 978-0-660-02766-1
</lines>

Aussi offert en français sous le titre *Audit de la gestion des biens de technologie de l'information*.