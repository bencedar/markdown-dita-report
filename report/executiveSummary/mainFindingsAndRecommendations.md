# Main Findings and Recommendations

## Governance

Roles, responsibilities, and accountabilities have changed on some aspects of IT asset management and are not reflected in the Industry Canada Asset Management Governance Structure. In addition, the assignment of these roles and responsibilities do not always take into consideration adequate segregation of duties.

Various IC initiatives, in support of the government-wide objective mentioned above in section 1.1, resulted in changes to the CIO’s roles, responsibilities, processes, and activities it carries out.
    * The DSR project provided the CIO with an opportunity to develop new processes, activities, and supporting tools regarding software procurement and management.
    * In October 2013, the CIO took on the responsibility for procuring desktop software within the Department.
    * In addition, the CIO created a baseline inventory listing of software on IC staff computers.

The audit found that while some business units continue to collect and record information of software purchases (including licenses); others believe that this responsibility was transferred to the CIO when they created the baseline inventory listing of software. There is confusion among some IC staff in relation to their roles and responsibilities as they pertain to software tracking.

Information on roles, responsibilities and accountabilities regarding disposal activities for IT hardware assets, including secure destruction, is not reflected in the Asset Management Governance Structure document. In addition, the audit found that some roles, responsibilities and accountabilities associated with secure destruction do not take into consideration adequate segregation of duties.

> ***Recommendation 1***:
> * CMS should ensure that during their three-year review cycle this fiscal year, the Asset Management Governance Structure document is updated to to reflect the current roles and responsibilities of all internal stakeholders and re-align, where needed, some roles and responsibilities, acknowledging adequate segregation of duties.
> * CMS, in collaboration with the CIO, should communicate these updates to IC staff.

There are review processes in place to update IC policies, directives, procedures and guidelines. The documents, however, do not reflect current practices. As a result, some of them are outdated and gaps exist.

The audit found that changes related to IT asset management and procurement were not reflected in neither CMS nor CIO policies, directives, procedures and guidelines. Current governing documents are outdated and some gaps exist. As well, it was unclear how CMS and CIO collaborate for the purpose of meeting client needs in the field of IT asset management.

CMS confirmed that they will review and update their policies and directives during 2015-16, as part of their established three year review cycle. Within the CIO, the review and update process of their specific governing documents occurs on an as needed basis. CIO’s intention is to update them as soon as it is feasible.

> ***Recommendation 2***:   
> * CMS should ensure that during their three-year review cycle this fiscal year, departmental policies, directives and guidelines related to IT asset management are updated, in collaboration with CIO, to better support IC staff in fulfilling their roles and responsibilities.
> * CIO should ensure that its specific governing documents related to IT asset management and procurement are updated in 2015-16, in collaboration with CMS, to better support IC staff in fulfilling their roles and responsibilities.
> * CMS and CIO should consider synchronizing their review processes so that information related to IT asset management is being updated on a regular basis and at the same time.

## Procurement

Computers and monitors are purchased through the mandatory Request for Volume Discount (RVD). Justifications are provided when these purchases do not go through this process.

The audit found that these purchases were in accordance with the CIO Directive on the IT Products and Services Procurement Process, which states that IC staff must use the mandatory RVD process to purchase planned and regular desktop computers and monitors or provide a rationale as to why the purchase could not go through the RVD process.

The CIO approval process is still being defined, documented, and communicated.

The CIO Directive on the IT Products and Services Procurement Process describes the Department’s process for the approval of procurement of IT products. According to this directive, IC staff needs to seek CIO approval prior to the procurement of non-RVD desktop computers and monitors, other IT hardware assets, as well as the procurement of software.

The audit found instances where no CIO approval was sought prior to the procurement of the IT asset. As well, IC staff was not always aware of the CIO approval requirement and was unclear as to what types of IT assets require CIO approval. The audit found that there is no clear departmental definition of what constitutes an IT hardware asset and would require a CIO approval. In addition, for software assets, there was insufficient guidance that explains what types of software are subject to the CIO approval.

Also, the audit found that, although in some instances where CIO approval was obtained prior to the purchase of the IT hardware or software, the process was not consistently followed as the request for CIO approval was made through e-mail or verbally, instead of using the E-Request form which was in effect during the period of the audit.

The CIO approval process has evolved over time with the implementation of E-Request forms to seek approval, the Helpdesk Expert Automation Tool (HEAT) Ticket System to track and manage CIO approval requests and status, and the creation of various IT analysis groups. To reflect these changes, the CIO is currently drafting an internal document titled IT Approval Process.

> ***Recommendation 3***: The CIO should:
> * Complete drafting their internal procedures and reflect them in the IT Approval Process document and communicate it to CIO staff; and
> * Improve its governing documents related to the CIO approval process for the procurement of the IT hardware and software and communicate the changes to IC staff.

## Tracking and Updating IT Asset in Management Systems

IT hardware assets are barcoded, tracked within the asset management system, and kept up-to-date through a combination of formal and ad-hoc activities.

The audit found that all IT hardware assets were barcoded and tracked in the PMM. In addition, updates of key information (e.g. changes of location and personnel) had occurred in the PMM for most of the IT hardware assets prior to the commencement of the annual asset verification exercise.

The annual asset verification exercise is conducted in accordance with departmental and Government of Canada requirements. As part of this exercise, the Department has implemented controls to mitigate the risks associated with some activities assigned to custodians. In addition, some good practices were adopted such as the use of a designated person to coordinate the exercise on behalf of all custodians within a business unit and then to liaise with CMS and the involvement of other IC staff to help provide independence from the custodian of the account.

The audit found that custodians are the ones carrying out the annual asset verification exercise and that most of the custodians performed both the inventory taking and record keeping functions. To compensate for this lack of segregation of duties, the Department has implemented controls within this exercise.

Not enough attention is given to the sensitivity of information on hardware assets with data storage capability declared as missing during the annual asset verification exercise.

The audit found that follow-up activities on IT hardware assets declared as missing do not include an assessment on the sensitivity of information stored on those with data storage capability. As well, no guidance exists as part of the annual asset verification exercise to assist IC staff on the need for such an assessment.

As a result, sensitive information that may exist on missing IT hardware assets with data storage capability would not be identified and appropriate follow-up steps would not be taken which is contrary to IC and GC requirements concerning the protection of information throughout its lifecycle.

> ***Recommendation 4***: CMS should update its documentation related to the annual asset verification exercise (including training material and procedures) to ensure attention is given by appropriate personnel to the sensitivity of information on missing IT assets with data storage capability.

The tracking of software is not performed consistently across the Department. Some initiatives have been undertaken to improve the monitoring of software installation.

The audit found that there is no common departmental IT asset management system to track software. As well, tracking of software, including licenses and renewals, by IC staff within the Department is not performed consistently.

In addition, the audit found that the CIO is tracking software for its own sector, SITT and CIPO, as well as for some departmental corporate software. CIO staff also stated that they are not responsible for tracking all software on behalf of the Department. CIO also acknowledged that the activities arising from the DSR project and the centralization of desktop software procurement within CIO have contributed to the confusion around software tracking responsibilities across the Department.

The CIO is creating a MS Access database to track and manage the CIO, SITT and CIPO’s software in order to support activities related to procurement, license renewal, upgrades and maintenance. In addition, activities are being undertaken to help mitigate the risk of exposure to potential liability by ensuring that legal ownership of software can be demonstrated.

> ***Recommendation 5***: CIO, in collaboration with CMS, should require that departmental software (including licenses and renewals) be tracked in a centralized database to ensure software tracking activities meet the operational needs.

## Disposal Activities

Some aspects of the disposal process are still to be defined, including those activities related to the sensitivity of information and secure destruction.

A variety of disposal mechanisms are available to IC staff. The audit found that the Computer and Internet Access program was used by IC staff as the first option for disposal, which is consistent with both IC and TB requirements.

The IC Standards and Guidelines for Disposal of Surplus Electronic and Electrical Equipment identified the procedures to follow when disposing surplus IT assets. However, it does not always clearly explain how to document these activities. As such, evidence was not always available to demonstrate that the Responsibility Centre Managers (RCM) authorized the disposals. In addition, the assessment of sensitivity of information on IT assets with data storage capability, which would determine whether the IT asset needs to be wiped or destroyed, was not always documented by the RCM.

The audit also found that documented IC guidance regarding secure destruction was insufficient and unclear. There was variation in the manner in which requests were made for proceeding with secure destruction and IC staff was unclear on the process they were expected to follow, including whom to contact.

> ***Recommendation 6***: The CIO, in collaboration with CMS, should better define, document, and communicate the disposal process including those activities related to secure destruction and consideration of sensitivity of information.

## Lost and Stolen IT Hardware Assets

Lost and stolen hardware assets are reported in a timely manner and the Department is working on refining its approach for assessing if sensitive information exists on these IT assets.

The audit found that identification and timely reporting of lost or stolen IT assets is promoted within IC as there are various channels through which the reporting of a lost or stolen IT asset can occur.

The audit also found that coordination occurs between CMS and the CIO regarding reported lost or stolen assets to ensure that appropriate SSD and CIO IT Security personnel are aware and involved when needed. It was also noted that lost or stolen IT assets were reported and responded to by SSD within a week of the incident occurring.