# Conformance with Professional Standards

This audit was conducted in accordance with the *Internal Auditing Standards* for the Government of Canada, as supported by the results of the Audit and evaluation Branche's quality assurance and improvement program.