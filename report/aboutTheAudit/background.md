# Background

In accordance with the approved Industry Canada (IC) 2014-15 to 2016-17 Multi-Year Risk-Based Internal Audit Plan, the Audit and Evaluation Branch (AEB) undertook an audit of Information Technology (IT) Asset Management.

The management of assets is directed by Treasury Board (TB) Policy Framework for the Management of Assets and Acquired Services and is complemented by additional TB direction addressing IT asset management. This includes the TB Policy Framework for Information and Technology; Policy on the Management of Materiel; Guide to Management of Materiel; Operational Security Standard on Physical Security; Policy on Accounting for Inventories; and the Directive on the Disposal of Surplus Materiel.

Accordingly, the Deputy Head of Industry Canada (IC) is accountable and responsible for implementing an effective management framework, including departmental procedures, processes, and systems that demonstrate how IC is managing its assets and for the effective management of information and technology throughout the Department. The Chief Financial Officer is accountable for ensuring an effective asset management framework is in place.

In support of meeting TB requirements, IC has implemented a framework for managing its assets (including IT assets) comprised of these key departmental policies, procedures, processes:
* Asset Management Governance Structure;
* Asset Management Policy (POL 016);
* Software Asset Management Policy (POL 019);
* Departmental Security Policy;
* Framework, Guidelines, and Procedures for the Annual Asset Verification Exercise;
* Directive on IT Products and Services Procurement Process;
* Draft IT Approval Process; and
* Guideline for the Disposal of Federal Surplus Electronic and Electrical Equipment.

In addition, IC uses the Plant Maintenance Module (PMM) within the Integrated Financial and Materiel System (IFMS) to record and track all barcoded assets within the Department including IT hardware assets. The total value of barcoded departmental IT hardware assets is not readily available from PMM as there is a lack of clear definition of what constitutes an IT hardware asset as further explained in section 3.2 of the report.

Key roles and responsibilities for IC staff involved in IT asset management and procurement activities are described below:

Corporate Management Sector (CMS):
* The Corporate Finance, Systems, and Procurement Branch is the functional authority for the management of departmental assets. This branch develops and communicates IC policies, directives and procedures that comply with TB requirements, and maintains a central inventory database of departmental assets. The Contracts and Materiel Management (CMM) and Corporate Finance groups within this branch are responsible for providing functional direction, advice and guidance in all areas of the materiel management life cycle and lead the annual asset verification exercise.
* The Security Services Directorate (SSD) is responsible for providing direction on the safeguarding of IC information and assets from compromise, and for investigating lost or stolen assets with collaboration from Chief Information Office (CIO), IT Security.

The CIO Sector is responsible for: providing direction and approval for the procurement of IT Products (hardware and software) and Services; coordinating the departmental Request for Volume Discount (RVD) procurement process for desktop computers and monitors; and, carrying out activities related to disposals, particularly data wiping and secure destruction.

For each sector and branch:
* Assistant Deputy Ministers and equivalents promote and support departmental initiatives related to asset management (with support from DGs, Directors, Responsibility Centre Managers (RCMs), and Supervisors) to ensure effective integration of roles and responsibilities for those involved in asset management activities within their respective organizations.
* RCMs, Asset Managers and Custodians are responsible for the day-to-day application of policies and procedures related to asset management (e.g. procurement, tracking of IT assets, annual asset verification, and disposals).

IT assets represent an essential component of the Government of Canada’s (GC) strategy to address challenges related to increasing productivity and enhancing services to the public for the benefit of citizens, businesses, and employees. As such, IT is changing significantly across the GC. Major initiatives, such as the creation of Shared Services Canada (SSC), is a move towards the GC’s objective of having a government-wide, standardized, centralized approach to managing its IT infrastructure, including supplying and supporting software and IT hardware assets. Two Orders in Council (OIC) were released in 2013 to authorize the transfer of duties from departments to SSC related to the acquisition and provision of hardware and software for end user devices. While the first OIC has been carried out, the second OIC which requires SSC to provide services for IT hardware and software assets is not yet implemented and IC is still managing its IT assets.

To incorporate these significant changes within its operational business environment, CIO senior management acknowledges the need of having a greater partnership between business units, the CIO and SSC. A longer-term priority of centralizing the management of IT hardware and software assets was also adopted by IC as a pre-cursor to the government-wide centralization approach.

The Department launched the Organizational Renewal and Business IT Transformation (ORBITT) initiative, which consolidated some IT resources from the Spectrum, Information Technologies and Telecommunications Sector (SITT) and the Canadian Intellectual Property Office (CIPO) within the CIO Sector. As part of this consolidation effective April 1st, 2014, the CIO became responsible for carrying out custodian services of some IT assets on behalf of CIPO and SITT.

Furthermore, the CIO has undertaken the Desktop Software Renewal (DSR) project to renew IC’s aging desktop computer operating system and related software by April 2014. In parallel with the DSR project, in October 2013, the CIO took on the responsibility for procuring desktop software within the Department.